package com.devcamp.task62jpaprovincerelationship.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62jpaprovincerelationship.model.District;
import com.devcamp.task62jpaprovincerelationship.model.Province;

public interface IDistrictRepository extends JpaRepository<District, Long>{
    District findById(int id);
    List<District> findByProvinceId(int id);
    Optional<Province> findByIdAndProvinceId(Long id, Long instructorId);
}
