package com.devcamp.task62jpaprovincerelationship.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62jpaprovincerelationship.model.District;
import com.devcamp.task62jpaprovincerelationship.model.Province;
import com.devcamp.task62jpaprovincerelationship.repository.IDistrictRepository;
import com.devcamp.task62jpaprovincerelationship.repository.IProvinceRepository;
import com.devcamp.task62jpaprovincerelationship.services.DistrictService;
import com.devcamp.task62jpaprovincerelationship.services.ProvinceService;

@RestController
@CrossOrigin
@RequestMapping("/district")
public class DistrictController {
    @Autowired
    ProvinceService provinceService;
    @Autowired
    DistrictService districtService;
    //get all provinces lisst
    @GetMapping("/all")
    public ResponseEntity<List<District>> getAllDistricts(){
        try {
            return new ResponseEntity<>(districtService.getAllDistricts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    //get district by province id
    @GetMapping("/search")
    public ResponseEntity<Set<District>> getDistrictsByProvinceIdApi(@RequestParam(value = "provinceId") int id){
        try {
            Set<District> provinceDistricts = provinceService.getDistrictsByProvinceId(id);
            if (provinceDistricts != null ){
                return new ResponseEntity<>(provinceDistricts, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //get district detail by id
    @Autowired
    IDistrictRepository districtRepository;
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getDistrictById(@PathVariable int id){
        District district = districtRepository.findById(id);
        if (district != null){
            return new ResponseEntity<>(district, HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    //create new district with province id
    @Autowired
    IProvinceRepository provinceRepository;
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createDistrict(@PathVariable("id") int id, @RequestBody District pDistrict){
        try {
            Province province = provinceRepository.findById(id);
            if (province != null){
                District newDistrict = new District();
                newDistrict.setName(pDistrict.getName());
                newDistrict.setPrefix(pDistrict.getPrefix());
                newDistrict.setProvince(province);
                District _district = districtRepository.save(newDistrict);
                //District _district = districtRepository.save(pDistrict);
                return new ResponseEntity<>(_district, HttpStatus.CREATED);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //update district
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateDistrict(@PathVariable(name="id")int id, @RequestBody District pDistrictUpdate){
        try {
           District district = districtRepository.findById(id);
            if (district != null){
                district.setName(pDistrictUpdate.getName());
                district.setPrefix(pDistrictUpdate.getPrefix());
                district.setProvince(pDistrictUpdate.getProvince());
                
                return new ResponseEntity<>(districtRepository.save(district), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     //delete district by Id
     @DeleteMapping("/delete/{id}")
     public ResponseEntity<District> deleteDistrictById(@PathVariable("id") int id) {
        try {
            District district = districtRepository.findById(id);
            districtRepository.delete(district);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
     }
}
